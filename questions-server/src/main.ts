import { createServer } from "http";
import { Server, Socket } from "socket.io";
import { Duel } from "./duel";
import { Scores } from "./scores";
import { Solo } from "./solo";

const DUEL = new Duel();
const SCORES = new Scores();
const SOLO = new Solo();

const httpServer = createServer();
const io = new Server(httpServer, {
  cors: {
    origin: [
      "http://localhost:3000",
      "https://electron.massiveballs.fr",
      "http://electron.massiveballs.fr",
    ],
    methods: ["GET", "POST"],
  },
});

io.on("connection", (socket: Socket) => {
  console.log("Connection");
  setInterval(
    () =>
      socket.emit("game tick", {
        duel: DUEL.toJson(),
        scores: SCORES.toJson(),
        solo: SOLO.toJson(),
      }),
    1000 / 100
  );

  socket.on("duel start game", () => {
    console.log("Starting game");
    DUEL.startGame();
  });
  socket.on("duel reset game", () => {
    console.log("Resetting game");
    DUEL.resetGame();
  });
  socket.on("duel pause game", () => {
    console.log("Pausing game");
    DUEL.pauseGame();
  });
  socket.on("duel buzz p1", () => {
    console.log("Buzz player 1");
    DUEL.buzz(0);
  });
  socket.on("duel buzz p2", () => {
    console.log("Buzz player 2");
    DUEL.buzz(1);
  });
  socket.on("duel correct", () => {
    console.log("Correct");
    DUEL.answer(true);
  });
  socket.on("duel pas correct", () => {
    console.log("Incorrect");
    DUEL.answer(false);
  });
  socket.on("duel alterner main", () => {
    console.log("alternage de main");
    DUEL.alternerMain();
  });
  socket.on("duel resetTimer", () => {
    console.log("resetTimer");
    DUEL.resetTimer();
  });
  socket.on("duel setScore", (newScore) => {
    console.log("duel setSecore");
    DUEL.setScore(newScore.player, newScore.score);
  });

  socket.on("scores reset", () => {
    console.log("reset de scores");
    SCORES.resetGame();
  });
  socket.on("scores buzz", (player) => {
    console.log("setTour " + player);
    SCORES.setTour(player);
  });
  socket.on("scores answer", (answer) => {
    console.log("answer " + answer);
    SCORES.answerQuestion(answer);
  });
  socket.on("scores set", (obj) => {
    console.log("scores set " + obj);
    SCORES.setScore(obj.player, obj.score);
  });

  socket.on("solo reset", () => {
    console.log("solo de reset");
    SOLO.resetGame();
  });
  socket.on("solo start", () => {
    console.log("solo start");
    if (SOLO.state === "PAUSED" || SOLO.state === "NOT_STARTED") {
      SOLO.start();
    } else {
      SOLO.pauseGame();
    }
  });
  socket.on("solo setTheme", (e) => {
    console.log("solo setTheme " + e);
    SOLO.setTheme(e);
  });
  socket.on("solo answer", (answer) => {
    console.log("solo answer " + answer);
    SOLO.answer(answer);
  });
});

httpServer.listen(6001);
