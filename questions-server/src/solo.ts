export class Solo {
  max = 0;
  scoreActuel = 0;
  time_start = new Date();
  timer = 0;
  theme = "";
  state: "NOT_STARTED" | "STARTED" | "ENDED" | "PAUSED" = "NOT_STARTED";

  start() {
    this.max = 0;
    this.scoreActuel = 0;
    this.time_start = new Date();
    this.state = "STARTED";
    this.startTimer();
  }

  setTheme(theme: string) {
    this.theme = theme;
  }

  resetGame() {
    this.max = 0;
    this.scoreActuel = 0;
    this.state = "NOT_STARTED";
    this.theme = "";
    this.resetTimer();
  }

  pauseGame(): void {
    if (this.state === "PAUSED") {
      this.startTimer();
      this.state = "STARTED";
    } else {
      this.pauseTimer();
      this.state = "PAUSED";
    }
  }

  pauseTimer(): void {
    this.timer += new Date().getTime() - this.time_start.getTime();
  }
  startTimer(): void {
    this.time_start = new Date();
  }

  resetTimer(): void {
    this.timer = 0;
  }

  answer(ok: boolean) {
    if (ok) {
      this.scoreActuel = this.scoreActuel + 1;
      this.max = Math.max(this.max, this.scoreActuel);
    } else {
      this.scoreActuel = 0;
    }
  }

  getTimer(): number {
    // const now = new Date();
    // return this.time_start.getTime() - now.getTime();
    if (this.state !== "STARTED") return this.timer;
    else return new Date().getTime() - this.time_start.getTime() + this.timer;
  }

  toJson() {
    return {
      max: this.max,
      scoreActuel: this.scoreActuel,
      timer: this.getTimer(),
      state: this.state,
      theme: this.theme
    };
  }
}
