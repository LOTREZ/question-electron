export class Scores {
  scores = [0, 0, 0, 0];
  tours = [0, 0, 0, 0];
  currentTour = null;

  resetGame(): void {
    this.scores = [0, 0, 0, 0];
    this.tours = [0, 0, 0, 0];
  }

  setTour(player: number): void {
    this.tours[player] = 1;
    this.currentTour = player;
  }

  answerQuestion(ok: boolean): void {
    this.scores[this.currentTour] += ok ? 1 : 0;
    this.currentTour = null;
    this.tours = [0, 0, 0, 0];
  }

  setScore(player: number, score: number) {
    this.scores[player] = score;
  }

  toJson(): any {
    return {
      scores: this.scores,
      tours: this.tours
    };
  }
}
