export class Duel {
  turn: 0 | 1 | 2;
  scores: number[];
  time_start: Date;
  state: "NOT_STARTED" | "STARTED" | "ENDED" | "PAUSED";
  timer: number;
  timerInterval: NodeJS.Timeout;
  main: number[];
  secondesParCase = 7;
  constructor() {
    this.turn = 1;
    this.state = "NOT_STARTED";
    this.main = [1, 0, 1, 0];
    this.scores = [0, 0];
    this.time_start = new Date();
    this.timer = 0;
  }

  startGame(): void {
    this.time_start = new Date();
    // this.resetGame();
    this.startTimer();
    this.state = "STARTED";
  }

  resetGame(): void {
    this.pauseTimer();
    this.resetTimer();
    this.resetMain();
    this.turn = 2;
    this.state = "NOT_STARTED";
    this.scores = [0, 0];
  }

  getTimer(): number {
    // const now = new Date();
    // return this.time_start.getTime() - now.getTime();
    if (this.state !== "STARTED") return this.timer;
    else return new Date().getTime() - this.time_start.getTime() + this.timer;
  }

  resetMain(): void {
    this.main = [1, 0, 1, 0];
  }

  resetTimer(): void {
    this.timer = 0;
  }

  alternerMain(): void {
    this.main = this.main.map((i) => (i = i === 0 ? 1 : 0));
  }

  pauseGame(): void {
    if (this.state === "PAUSED") {
      this.startTimer();
      this.state = "STARTED";
    } else {
      this.pauseTimer();
      this.state = "PAUSED";
    }
  }

  buzz(player: 0 | 1 | 2): void {
    this.turn = player;
    this.pauseGame();
  }

  answer(correct: boolean): void {
    if (correct) {
      this.scores[this.turn] += this.getCurrentNumber();
      this.endGame();
    } else this.main[this.getCurrentNumber() - 1] = this.turn === 1 ? 0 : 1;
    // this.pauseGame();
  }

  endGame(): void {
    this.resetTimer();
    this.resetMain();
    // this.pauseTimer();
    // this.stopwatch.stop();
    this.state = "ENDED";
  }

  setScore(player: number, score: number) {
    this.scores[player] = score;
  }

  pauseTimer(): void {
    this.timer += new Date().getTime() - this.time_start.getTime();
  }

  startTimer(): void {
    this.time_start = new Date();
  }

  addScore(player: number): void {
    this.scores[player] += Math.ceil(this.getTimer());
  }

  getCurrentNumber(): number {
    const number =
      5 + Math.floor(-this.getTimer() / (this.secondesParCase * 1000));
    if (number === 0) this.endGame();
    return number;
  }

  toJson(): GameJson {
    return {
      turn: this.turn,
      scores: this.scores,
      time_start: this.time_start,
      state: this.state,
      timer: this.getTimer(),
      currentNumber: this.getCurrentNumber(),
      main: this.main
    };
  }

  toString(): string {
    return JSON.stringify(this.toJson());
  }
}

interface GameJson {
  turn: 0 | 1 | 2;
  scores: number[];
  time_start: Date;
  state: "NOT_STARTED" | "STARTED" | "ENDED" | "PAUSED";
  timer: number;
  currentNumber: number;
  main: number[];
}
