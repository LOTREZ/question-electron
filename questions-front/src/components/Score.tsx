import React from "react";
import "./style.css";
export default function Score({
  score,
  selected,
}: {
  score: number;
  selected?: boolean;
}) {
  return (
    // <div style={{ fontSize: 40, fontWeight: "bold", color: "blue" }}>
    //   {score}
    // </div>
    <svg
      style={{
        width: 200,
        // height: "fit-content",
        marginBottom: 50,
        boxShadow: "10px 10px 20px 0px rgb(0 0 0 / 60%)",
      }}
      version="1.1"
      id="Calque_1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      viewBox="0 0 1000 500"
      enableBackground="new 0 0 1000 500"
    >
      <rect x="183" y="23.38" fill="none" width="634.09" height="453.41" />
      <rect x="-0.05" y="-0.08" fill="#C39FCA" width="1000" height="500" />
      <rect x="25" y="25" fill="#905AA1" width="950" height="450" />

      <linearGradient
        id="SVGID_1_"
        gradientUnits="userSpaceOnUse"
        x1="17.99"
        y1="10.1705"
        x2="987.8282"
        y2="492.839"
      >
        <stop offset="0.0401" stopColor="#e7bff9" />

        <stop offset="0.9996" stopColor="white" />
      </linearGradient>
      <path
        d="M0.05,0.08v500h1000v-500H0.05z M925,450H50V50H950V450z"
        fill="url(#SVGID_1_)"
      />
      <rect x="236.98" y="55.76" fill="none" width="526.14" height="388.64" />
      <text
        transform="matrix(1 0 0 1 375.0452 410.6069)"
        fill={selected ? "#77dd77" : "white"}
        textAnchor="center"
        fontFamily="Kimberley"
        fontSize="423px"
        style={{
          transition: "color 0.5s ease",
        }}
      >
        {score}
      </text>
    </svg>
  );
}
