import React, { useState } from "react";

export default function Button({
  texte,
  onClick,
  disabled,
}: {
  texte: string;
  onClick?: React.MouseEventHandler;
  disabled?: boolean;
}) {
  const [buttonHover, setButtonHover] = useState<boolean>(false);

  return (
    <button
      style={{
        fontSize: 18,
        textAlign: "center",
        backgroundColor: buttonHover ? "#217dbb" : "#3498db",
        paddingBottom: 17,
        paddingTop: 17,
        paddingLeft: 24,
        paddingRight: 24,
        cursor: "pointer",
        transition: "background-color 0.2s ease",
      }}
      onMouseEnter={() => setButtonHover(true)}
      onMouseLeave={() => setButtonHover(false)}
      onClick={onClick}
      disabled={disabled === undefined ? false : disabled}
    >
      {texte}
    </button>
  );
}
