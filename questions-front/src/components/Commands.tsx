import React, { useState } from "react";
import { ConnectionStatus, Game } from "../App";
import Button from "./Button";

export default function Commands({
  game,
  connectionStatus,
  startGame,
  resetGame,
  pauseGame,
  buzzP1,
  buzzP2,
  correct,
  incorrect,
  alternerMain,
  setNewScore,
  resetTimer,
}: {
  game?: Game;
  startGame: () => void;
  resetGame: () => void;
  pauseGame: () => void;
  buzzP1: () => void;
  buzzP2: () => void;
  correct: () => void;
  incorrect: () => void;
  resetTimer: () => void;
  alternerMain: () => void;
  setNewScore: (player: number, score: number) => void;
  connectionStatus: ConnectionStatus;
}) {
  const [button1Hover, setButton1Hover] = useState<boolean>(false);
  const [button2Hover, setButton2Hover] = useState<boolean>(false);
  return (
    <div
      style={{
        margin: 10,
        boxShadow: "0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)",
        padding: 10,
        height: 350,
        width: 800,
        transition: "background-color 0.5s ease",
        backgroundColor:
          connectionStatus === "NOT_CONNECTED" ? "#e53935" : "#7cb342",
      }}
    >
      <div>STATUS: {connectionStatus}</div>
      <div>{JSON.stringify(game)}</div>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-around",
          width: "100%",
          marginTop: 15,
          marginBottom: 15,
        }}
      >
        <Button
          texte={"BUZZ P1"}
          disabled={
            game?.main[game?.currentNumber - 1] !== 0 || game.state === "PAUSED"
          }
          onClick={buzzP1}
        />
        <Button
          texte={"BUZZ P2"}
          disabled={
            game?.main[game?.currentNumber - 1] !== 1 || game.state === "PAUSED"
          }
          onClick={buzzP2}
        />
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
          width: "100%",
          marginTop: 15,
          marginBottom: 15,
        }}
      >
        <Button
          texte={"CORRECT"}
          disabled={game?.state === "NOT_STARTED" || game?.state === "STARTED"}
          onClick={correct}
        />
        <Button
          texte={"PAS BON"}
          disabled={game?.state === "NOT_STARTED" || game?.state === "STARTED"}
          onClick={incorrect}
        />
      </div>

      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-around",
          width: "100%",
          marginTop: 15,
          marginBottom: 15,
        }}
      >
        <Button texte={"RESET GAME"} onClick={resetGame} />
        <Button
          texte={"ALTERNER MAIN"}
          onClick={alternerMain}
          disabled={game?.state !== "NOT_STARTED" && game?.state !== "ENDED"}
        />
        <Button texte={"START GAME"} onClick={startGame} disabled={game?.state === "STARTED"}/>
        <Button
          texte={game?.state === "PAUSED" ? "RESUME GAME" : "PAUSE GAME"}
          onClick={pauseGame}
        />
        <Button
          texte={"RESET TIMER"}
          onClick={resetTimer}
        />
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-around",
          width: "100%",
          marginTop: 15,
          marginBottom: 15,
        }}
      >
        <input style={{width: 150, height: 50, fontSize: 40}} type="number" onChange={(e) => setNewScore(0, parseInt(e.currentTarget.value))} value={game?.scores[0]}/>
        <input style={{width: 150, height: 50, fontSize: 40}} type="number" onChange={(e) => setNewScore(1, parseInt(e.currentTarget.value))} value={game?.scores[1]}/>
      </div>
    </div>
  );
}
