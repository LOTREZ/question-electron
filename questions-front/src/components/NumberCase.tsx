import React, { useEffect, useState } from "react";
import { Game } from "../App";

export default function NumberCase({
  number,
  currentTime,
  game,
  style,
}: {
  number: number;
  currentTime: number;
  game: Game;
  style?: React.CSSProperties;
}) {
  const [loadingHeight, setLoadingHeight] = useState<number>(0);
  const [loadingY, setLoadingY] = useState<number>(0);
  // const taille = 60 + 10 * number;
  const taille = 100;
  const secondesParCase = 7;
  // si on est a - 5000 de timer on devrait etre au premier truc

  const caseMillis = 1000 * secondesParCase;

  useEffect(() => {
    // calculer height
    var height = 0;

    // cas supérieur à
    if (game.currentNumber < number) height = 0;
    else if (game.currentNumber > number) height = 100;
    else height = 100 - ((game.timer % caseMillis) / caseMillis) * 100;
    setLoadingHeight(height);
    setLoadingY(100 - height);
  }, [game]);

  return (
    <svg
      version="1.1"
      x="0px"
      y="0px"
      viewBox={"0 0 55.650237 " + taille}
      id="svg77"
      style={{
        ...style,
        ...{
          borderTop: number === 4 ? "6px solid rgb(231, 191, 249)" : "",
          borderBottom: number === 1 ? "6px solid rgb(231, 191, 249)" : "",

          boxShadow: "0px 3px 10px 0px rgba(0,0,0,0.4)",
          width: "auto",
          // borderTop: "1px solid black",
          transition: "margin 0.2s ease",
          zIndex: number,
        },
      }}
      // width="55.650238"
    >
      <g id="Guide" transform="translate(-324.78,-125.19)"></g>
      <g
        id="Calque_5"
        transform="matrix(0.13744193,0,0,0.13744193,-44.63839,-17.206356)"
      >
        <g id="Calque_4">
          <rect
            x="324.78"
            y="125.19"
            fill="#e7bff9"
            width="24.98"
            height="727.58002"
            id="rect67"
          />
          {/* {number === 1 && (
            <rect
              x="300"
              y="830"
              fill="#e7bff9"
              width="700.94"
              height="24.98"
              id="rect74"
            />
          )} */}
          <rect
            x="704.70001"
            y="125.19"
            fill="#e7bff9"
            width="24.98"
            height="727.58002"
            id="rect69"
          />
          <rect
            x="349.76001"
            y="125.19"
            opacity="0.51"
            fill="#ca72f1"
            width="354.94"
            height="727.58002"
            id="rect71"
          />
          <rect
            x="349.76001"
            y="125.19"
            fill="#ca72f1"
            width="354.94"
            height="727.58002"
            id="rect73"
          />
        </g>
      </g>
      {/* <g id="layer1" opacity="0.4"> */}
      <rect
        id="rect129"
        width="48.846111"
        height={loadingHeight}
        x="3.4333007"
        y={loadingY}
        // style={{ transition: "height 0.1s ease" }}
        fill="rgb(144,90,161)"
      />
      {/* </g> */}
      <text
        id="text123"
        fontSize="40px"
        fontStyle="normal"
        fontWeight="normal"
        fontFamily="Kimberley"
        white-space="pre"
        textAnchor="middle"
        fill="white"
        fillOpacity="1"
        stroke="none"
        x="27.825"
        y={taille / 2 + 15}
      >
        {number}
        {/* <tspan x="15.110352" y="36.493213">
          <tspan textAnchor="middle">{number}</tspan>
        </tspan> */}
      </text>
    </svg>
  );
}
