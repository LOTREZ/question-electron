import React from "react";

export default function Theme({ text }: { text: string }) {
  return (
    <svg
      style={{
        // width: 200,
        // marginBottom: 50,
        height: "70px",
        // margin: 50,
        boxShadow: "10px 10px 20px 0px rgb(0 0 0 / 60%)",
      }}
      version="1.1"
      id="Calque_1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      viewBox="0 0 8000 500"
      enableBackground="new 0 0 8000 500"
    >
      <rect x="183" y="23.38" fill="none" width="5000" height="453.41" />
      <rect x="-0.05" y="-0.08" fill="#C39FCA" width="8000" height="500" />
      <rect x="25" y="25" fill="#905AA1" width="7950" height="450" />

      <linearGradient
        id="SVGID_1_"
        gradientUnits="userSpaceOnUse"
        x1="17.99"
        y1="10.1705"
        x2="987.8282"
        y2="492.839"
      >
        <stop offset="0.0201" stopColor="#e7bff9" />

        <stop offset="0.9996" stopColor="white" />
      </linearGradient>
      <path
        d="M0.05,0.08v500h8000v-500H0.05z M7925,450H50V50H7950V450z"
        fill="url(#SVGID_1_)"
      />
      <rect x="236.98" y="55.76" fill="none" width="2100.14" height="388.64" />
      {/* <g transform="translate(50,50)" width="8000"> */}
      <g width="8000" height="500" fill="none" transform="translate(50,50)">
        <rect width="8000" fill="none" height="500" />
        <text
          // transform="matrix(1 0 0 1 375.0452 410.6069)"
          fill={"white"}
          textAnchor="middle"
          alignmentBaseline="central"
          y="45%"
          x="50%"
          fontFamily="Kimberley"
          fontSize="350px"
          dominantBaseline="ideographic"
          style={{
            transition: "color 0.5s ease",
          }}
        >
          {text}
        </text>
      </g>
      {/* </g> */}
    </svg>
  );
}
