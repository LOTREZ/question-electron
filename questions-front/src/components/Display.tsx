import React from "react";
import { Game } from "../App";
import NumberCase from "./NumberCase";
import Score from "./Score";
import "./style.css";

export default function Display({ game }: { game: Game }) {
  const currentTime = 2.5;
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-evenly",
        alignItems: "flex-end",
        width: 1920,
        height: 1080,
      }}
    >
      <Score score={game.scores[0]} />
      <div
        style={{
          height: "100%",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          width: 150.21,
        }}
      >
        <NumberCase
          number={4}
          currentTime={currentTime}
          game={game}
          style={{
            marginLeft: game.main[3] === 1 ? 40 : 0,
            marginRight: game.main[3] === 0 ? 40 : 0,
          }}
        />
        <NumberCase
          number={3}
          currentTime={currentTime}
          game={game}
          style={{
            marginLeft: game.main[2] === 1 ? 40 : 0,
            marginRight: game.main[2] === 0 ? 40 : 0,
          }}
        />
        <NumberCase
          number={2}
          currentTime={currentTime}
          game={game}
          style={{
            marginLeft: game.main[1] === 1 ? 40 : 0,
            marginRight: game.main[1] === 0 ? 40 : 0,
          }}
        />
        <NumberCase
          number={1}
          currentTime={currentTime}
          game={game}
          style={{
            marginLeft: game.main[0] === 1 ? 40 : 0,
            marginRight: game.main[0] === 0 ? 40 : 0,
          }}
        />
      </div>
      <Score score={game.scores[1]} />
    </div>
  );
}
