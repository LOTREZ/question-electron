import React, { useEffect, useState } from 'react';
import { io } from 'socket.io-client';
import { ConnectionStatus, Game } from './App';
import Commands from './components/Commands';
import Display from './components/Display';

export default function Duel({socketURL}: {socketURL: string}){
	const [game, setGame] = useState<Game>();
  const [startGame, setStartGame] = useState<() => void>(
    () =>
      function () {
        console.log("Start game not yet defined");
      }
  );
  const [pauseGame, setPauseGame] = useState<() => void>(
    () =>
      function () {
        console.log("Pause game not yet defined");
      }
  );
  const [resetGame, setResetGame] = useState<() => void>(
    () =>
      function () {
        console.log("Reset game not yet defined");
      }
  );
  const [buzzP1, setBuzzP1] = useState<() => void>(
    () =>
      function () {
        console.log("buzzP1 not yet defined");
      }
  );
  const [buzzP2, setBuzzP2] = useState<() => void>(
    () =>
      function () {
        console.log("buzzP2 not yet defined");
      }
  );
  const [correct, setCorrect] = useState<() => void>(
    () =>
      function () {
        console.log("correct not yet defined");
      }
  );
  const [incorrect, setIncorrect] = useState<() => void>(
    () =>
      function () {
        console.log("correct not yet defined");
      }
  );
  const [resetTimer, setResetTimer] = useState<() => void>(
    () =>
      function () {
        console.log("resetTimer not yet defined");
      }
  );
  const [alternerMain, setAlternerMain] = useState<() => void>(
    () =>
      function () {
        console.log("alternerMain not yet defined");
      }
  );
  const [setNewScore, setSetNewScore] = useState<(player: number, score: number) => void>(
    () =>
      function () {
        console.log("setNewScore not yet defined");
      }
  );
  const [connectionStatus, setConnectionStatus] = useState<ConnectionStatus>(
    "NOT_CONNECTED"
  );

    function handleKeyPress(this: Document, ev: KeyboardEvent){

    }

    const handleKey2 = (event: React.KeyboardEvent) => {
      console.log("cum:" + event.keyCode)
      if(game === undefined) return;
      // ev.stopPropagation()
      console.log({currentNumber: game?.currentNumber, main: game.main, corres: game?.main[game?.currentNumber - 1]})
      console.log("premier cas: " + (game?.main[game?.currentNumber - 1] === 0 && game.state !== "PAUSED"))
      console.log("deuxieme cas: " + (game?.main[game?.currentNumber - 1] === 1 && game.state !== "PAUSED"))
      switch (event.key) {
        case "1":
          if(game?.main[game?.currentNumber - 1] === 0 && game.state !== "PAUSED") buzzP1()
          break;
        case "2":
          if(game?.main[game?.currentNumber - 1] === 1 && game.state !== "PAUSED") buzzP2()
          break;
        case "a":
          if(game.state === "PAUSED") correct()
          break;
        case "z":
          if(game.state === "PAUSED") incorrect()
          break;
        case " ":
          if(game.state !== "STARTED") startGame()
          else pauseGame()
          break;
  
        default:
          break;
      }
    }

    useEffect(() => {
      const socket = io(socketURL);
      
      // client-side
      socket.on("connect", () => {
        setConnectionStatus("CONNECTED");
        setStartGame(
          () =>
          function () {
            console.log("starting game");
            socket.emit("duel start game");
          }

      );
      setResetGame(
        () =>
          function () {
            console.log("reset game");
            socket.emit("duel reset game");
          }
      );
      setResetTimer(
        () =>
          function () {
            console.log("reset timer");
            socket.emit("duel resetTimer");
          }
      );
      setPauseGame(
        () =>
          function () {
            console.log("pausing game");
            socket.emit("duel pause game");
          }
      );
      setBuzzP1(
        () =>
          function () {
            console.log("buzzing1");
            socket.emit("duel buzz p1");
          }
      );
      setBuzzP2(
        () =>
          function () {
            console.log("buzzing2");
            socket.emit("duel buzz p2");
          }
      );
      setCorrect(
        () =>
          function () {
            console.log("correct");
            socket.emit("duel correct");
          }
      );
      setIncorrect(
        () =>
          function () {
            console.log("Incorrect");
            socket.emit("duel pas correct");
          }
      );
      setAlternerMain(
        () =>
          function () {
            console.log("alterner main");
            socket.emit("duel alterner main");
          }
      );
      setSetNewScore(
        () =>
          function (player: number, score: number) {
            console.log("setNewScire main");
            socket.emit("duel setScore", {player: player, score: score});
          }
      );
    });

    socket.on("disconnect", () => {
      setConnectionStatus("NOT_CONNECTED");
    });

    socket.on("game tick", (value) => setGame(value.duel));


    // document.addEventListener("keydown", handleKeyPress)
  }, []);

  return <div onKeyDown={handleKey2} tabIndex={0}>
	  {game?.timer !== undefined && <Display game={game} />}

      <Commands
        game={game}
        startGame={startGame}
        pauseGame={pauseGame}
        resetGame={resetGame}
        buzzP1={buzzP1}
        buzzP2={buzzP2}
        correct={correct}
        incorrect={incorrect}
        alternerMain={alternerMain}
        connectionStatus={connectionStatus}
        setNewScore={setNewScore}
        resetTimer={resetTimer}
      />
  </div>
}