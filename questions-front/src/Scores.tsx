import React, { useEffect, useState } from 'react';
import { io } from 'socket.io-client';
import { ConnectionStatus } from './App';
import Button from './components/Button';
import Score from './components/Score';

interface Scores{
	tours: number[]
	scores: number[]
}

export default function Scores({socketURL}: {socketURL: string}){

  const [buzz, setBuzz] = useState<(player: number) => void>(
    () =>
      function (player: number) {
        console.log("buzz not yet defined");
      }
  );
  const [answer, setAnswer] = useState<(ok: boolean) => void>(
    () =>
      function (ok: boolean) {
        console.log("answer not yet defined");
      }
  );
  const [resetGame, setResetGame] = useState<() => void>(
    () =>
      function () {
        console.log("resetGame not yet defined");
      }
  );
  const [setNewScore, setSetNewScore] = useState<(player: number, score: number) => void>(
    () =>
      function (player: number, score: number) {
        console.log("resetGame not yet defined");
      }
  );
  const [game, setGame] = useState<Scores>();

  const [connectionStatus, setConnectionStatus] = useState<ConnectionStatus>(
    "NOT_CONNECTED"
  );
    useEffect(() => {


      const socket = io(socketURL);
      
      // client-side
      socket.on("connect", () => {
        setConnectionStatus("CONNECTED");
		setResetGame(
			() =>
			function () {
				console.log("scores reset");
				socket.emit("scores reset");
			}
		);
		setBuzz(
			() =>
			function (player: number) {
				console.log("buzzing1");
				socket.emit("scores buzz", player);
			}
		);
		setAnswer(
			() =>
			function (ok: boolean) {
				console.log("answer: " + ok);
				socket.emit("scores answer", ok);
			}
		);
		setSetNewScore(
			(player: number, score: number) =>
			function (player: number, score: number) {
				console.log("scores set: " + player + score);
				socket.emit("scores set", {player: player, score:score});
			}
		);
    });

    socket.on("disconnect", () => {
      setConnectionStatus("NOT_CONNECTED");
    });

    socket.on("game tick", (value) => setGame(value.scores));

  }, []);

    const handleKey2 = (event: React.KeyboardEvent) => {
		console.log(event.key)
		if(game === undefined) return;
		switch (event.key) {
		case "&":
			if(game?.tours.filter((tour) => tour === 1).length === 0) buzz(0)
			break;
		case "é":
			if(game?.tours.filter((tour) => tour === 1).length === 0) buzz(1)
			break;
		case '"':
			if(game?.tours.filter((tour) => tour === 1).length === 0) buzz(2)
			break;
		case "'":
			if(game?.tours.filter((tour) => tour === 1).length === 0) buzz(3)
			break;
		case "a":
			if(game?.tours.filter((tour) => tour === 1).length > 0) answer(true)
			break;
		case "z":
			if(game?.tours.filter((tour) => tour === 1).length > 0) answer(false)
			break;

		default:
			break;
		}
    }

	return <div onKeyDown={handleKey2} tabIndex={0}>
		<div style={{
			display: "flex",
			flexDirection: "row",
			justifyContent: "space-evenly",
			alignItems: "flex-end",
			width: 1920,
			height: 1080,
		}}>
			<Score score={game?.scores[0] || 0} selected={game?.tours[0] === 1} />
			<Score score={game?.scores[1] || 0} selected={game?.tours[1] === 1} />
			<Score score={game?.scores[2] || 0} selected={game?.tours[2] === 1} />
			<Score score={game?.scores[3] || 0} selected={game?.tours[3] === 1} />
		</div>
		{game?.tours !== undefined && <div style={{
        margin: 10,
        boxShadow: "0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)",
        padding: 10,
        height: 300,
        width: 800,
        transition: "background-color 0.5s ease",
        backgroundColor:
          connectionStatus === "NOT_CONNECTED" ? "#e53935" : "#7cb342",
      }}>
		{JSON.stringify(game)}

			<div>
				<Button
				texte={"BUZZ 1"}
				onClick={() => buzz(0)}
				disabled={game?.tours.filter((tour) => tour === 1).length > 0}
				/>
				<Button
				texte={"BUZZ 2"}
				onClick={() => buzz(1)}
				disabled={game?.tours.filter((tour) => tour === 1).length > 0}
				/>
				<Button
				texte={"BUZZ 3"}
				onClick={() => buzz(2)}
				disabled={game?.tours.filter((tour) => tour === 1).length > 0}
				/>
				<Button
				texte={"BUZZ 4"}
				onClick={() => buzz(3)}
				disabled={game?.tours.filter((tour) => tour === 1).length > 0}
				/>
			</div>
			<div>
				<Button
				texte={"OK"}
				onClick={() => answer(true)}
				disabled={game?.tours.filter((tour) => tour === 1).length === 0}
				/>
				<Button
				texte={"INCORRECT"}
				onClick={() => answer(false)}
				disabled={game?.tours.filter((tour) => tour === 1).length === 0}
				/>
			</div>
			<div>
				<Button
				texte={"RESET"}
				onClick={() => resetGame()}
				/>
			</div>
			<div
			style={{
			display: "flex",
			flexDirection: "row",
			justifyContent: "space-around",
			width: "100%",
			marginTop: 15,
			marginBottom: 15,
			}}
		>
			<input style={{width: 150, height: 50, fontSize: 40}} type="number" onChange={(e) => setNewScore(0, parseInt(e.currentTarget.value))} value={game?.scores[0]}/>
			<input style={{width: 150, height: 50, fontSize: 40}} type="number" onChange={(e) => setNewScore(1, parseInt(e.currentTarget.value))} value={game?.scores[1]}/>
			<input style={{width: 150, height: 50, fontSize: 40}} type="number" onChange={(e) => setNewScore(2, parseInt(e.currentTarget.value))} value={game?.scores[2]}/>
			<input style={{width: 150, height: 50, fontSize: 40}} type="number" onChange={(e) => setNewScore(3, parseInt(e.currentTarget.value))} value={game?.scores[3]}/>
		</div>
		</div>}
		
	</div>
	
}