import React, { useEffect, useState } from "react";
import { io } from "socket.io-client";
import { ConnectionStatus } from "./App";
import Button from "./components/Button";
import Score from "./components/Score";
import Theme from "./components/Theme";

interface Game {
  max: number;
  scoreActuel: number;
  timer: number;
  theme: string;
  state: "NOT_STARTED" | "STARTED" | "ENDED" | "PAUSED";
}

export default function Solo({ socketURL }: { socketURL: string }) {
  const [game, setGame] = useState<Game>();
  const [answer, setAnswer] = useState<(ok: boolean) => void>(
    () =>
      function (ok: boolean) {
        console.log("answer not yet defined");
      }
  );
  const [setTheme, setSetTheme] = useState<(theme: string) => void>(
    () =>
      function () {
        console.log("setTheme not yet defined");
      }
  );
  const [resetGame, setResetGame] = useState<() => void>(
    () =>
      function () {
        console.log("resetGame not yet defined");
      }
  );
  const [startGame, setStartGame] = useState<() => void>(
    () =>
      function () {
        console.log("startGame not yet defined");
      }
  );
  const [connectionStatus, setConnectionStatus] =
    useState<ConnectionStatus>("NOT_CONNECTED");

  useEffect(() => {
    const socket = io(socketURL);

    // client-side
    socket.on("connect", () => {
      setConnectionStatus("CONNECTED");
      setResetGame(
        () =>
          function () {
            console.log("solo reset");
            socket.emit("solo reset");
          }
      );
      setStartGame(
        () =>
          function () {
            console.log("solo start");
            socket.emit("solo start");
          }
      );
      setAnswer(
        () =>
          function (ok: boolean) {
            console.log("answer: " + ok);
            socket.emit("solo answer", ok);
          }
      );
      setSetTheme(
        () =>
          function (theme: string) {
            socket.emit("solo setTheme", theme);
          }
      );
    });

    socket.on("disconnect", () => {
      setConnectionStatus("NOT_CONNECTED");
    });

    socket.on("game tick", (value) => setGame(value.solo));
  }, []);

  return (
    <div>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-between",
          alignItems: "flex-end",
          width: 1920,
          height: 1080,
        }}
      >
        <div
          style={{
            display: "flex",
            justifyContent: "flex-start",
            width: "100%",
            padding: 50,
          }}
        >
          <div style={{ marginLeft: 145, marginTop: 30 }}>
            <Score score={60 - Math.round((game?.timer || 0) / 1000)} />
          </div>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-evenly",
            alignItems: "baseline",
            width: "100%",
          }}
        >
          <Score score={game?.max || 0} />
          <Theme text={game?.theme || ""} />
          <Score score={game?.scoreActuel || 0} />
        </div>
      </div>
      {JSON.stringify(game)}
      {JSON.stringify(connectionStatus)}

      {game !== undefined && (
        <div
          style={{
            margin: 10,
            boxShadow: "0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)",
            padding: 10,
            height: 300,
            width: 800,
            transition: "background-color 0.5s ease",
            backgroundColor:
              connectionStatus === "NOT_CONNECTED" ? "#e53935" : "#7cb342",
          }}
        >
          {JSON.stringify(game)}

          <div>
            <Button
              texte={"OK"}
              onClick={() => answer(true)}
              disabled={game.state !== "STARTED"}
            />
            <Button
              texte={"INCORRECT"}
              onClick={() => answer(false)}
              disabled={game.state !== "STARTED"}
            />
          </div>
          <div>
            <Button texte={"START/PAUSE"} onClick={() => startGame()} />
            <Button texte={"RESET"} onClick={() => resetGame()} />
          </div>
          <div>
            <input
              type="text"
              onChange={(e) => setTheme(e.currentTarget.value.toString())}
            />
          </div>
        </div>
      )}
    </div>
  );
}
