import React, { useState } from "react";
import {
  Route, Switch
} from "react-router-dom";
import "./App.css";
import Duel from "./Duel";
import Scores from "./Scores";
import Solo from "./Solo";

export interface Game {
  turn: 0 | 1 | 2;
  scores: number[];
  time_start: Date;
  state: "NOT_STARTED" | "STARTED" | "ENDED" | "PAUSED";
  timer: number;
  currentNumber: number;
  main: number[];
}

export type ConnectionStatus = "CONNECTED" | "NOT_CONNECTED";

function App() {

  let appUrl: string;
  if(window.location.hostname === "localhost"){
    appUrl = "ws://localhost:6001"
  }else{
    appUrl = "wss://electron.massiveballs.fr"
  }

  
  const [socketURL, setSocketURL] = useState(appUrl);


  return (
    <div className="App" tabIndex={0}>
        <Switch>
          <Route exact path="/duel">
          <Duel socketURL={socketURL}/>
          </Route>
          <Route path="/scores">
            <Scores socketURL={socketURL}/>
          </Route>
          <Route path="/solo">
            <Solo socketURL={socketURL}/>
          </Route>
        </Switch>
    </div>
  );
}

export default App;
